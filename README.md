# Docker helper containers

Helper containers for the factory project, mostly intended as CI images, but versions may be upgraded. The containers are rebuild weekly.

1. [kubernetes-deploy](#kubernetes-deploy)
2. [firefox-testing](#firefox-testing)
3. [graal-11](#graal-11)
3. [java-kaniko](#java-kaniko)

## kubernetes-deploy
Image: [registry.gitlab.com/factory-org/tools/docker/kubernetes-deploy](https://gitlab.com/factory-org/tools/docker/container_registry/1143880) <br/>
Base: [alpine](https://hub.docker.com/_/alpine)

Intended for deployment of applications to kubernetes using helm or kubectl.

Includes:
 - kubectl
 - helm
 - sentry-cli

## firefox-testing
Image: [registry.gitlab.com/factory-org/tools/docker/firefox-testing](https://gitlab.com/factory-org/tools/docker/container_registry/2665876) <br/>
Base: [cypress/base:&lt;version&gt;](https://hub.docker.com/cypress/base)

Intended for testing with karma and cypress. Tests should use `FirefoxHeadless` in CI.

Includes:
 - Firefox
 - Dependencies for cypress

## graal-11
**Deprecated**, use https://github.com/graalvm/container/pkgs/container/graalvm-ce

## java-kaniko
Image: [registry.gitlab.com/factory-org/tools/docker/java-kaniko](https://gitlab.com/factory-org/tools/docker/container_registry/1355206) <br/>
Base: [azul/zulu-openjdk:15](https://hub.docker.com/r/azul/zulu-openjdk)

Intended for jvm compilation featuring [kaniko](https://github.com/GoogleContainerTools/kaniko)

Includes:
 - Azul Zulu 15
 - kaniko
